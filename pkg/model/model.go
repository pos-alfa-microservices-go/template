package model

import "github.com/google/uuid"

type Model struct {
	Id uuid.UUID
}
